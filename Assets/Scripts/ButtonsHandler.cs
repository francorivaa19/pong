﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsHandler : MonoBehaviour
{
    [SerializeField] private Button exitButton = null;
    [SerializeField] private Button playAgain = null;

    private Behaviour ball;

    [SerializeField] private GameObject gameOverScreen = null;

    private void Awake()
    {
        ball = GetComponent<Behaviour>();

        exitButton.onClick.AddListener(ExitGame);
        playAgain.onClick.AddListener(PlayAgainHandler);
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    private void PlayAgainHandler()
    {
        Time.timeScale = 1f;
        gameOverScreen.SetActive(false);
    }
}
