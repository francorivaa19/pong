﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovement : MonoBehaviour
{
    [SerializeField] private float speed = 0.0F;

    private Vector3 initialPos;
    private float horizontalWall = 6f;

    private Vector3 xPlus = new Vector3 (2, 0, 0);

    private void Start()
    {
        initialPos = transform.position;
    }

    private void Update()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;

        if (transform.position.y <= -horizontalWall)
            transform.position = initialPos += xPlus;
    }
}
