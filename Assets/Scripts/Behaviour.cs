﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Behaviour : MonoBehaviour
{
    #region fields

    //Speed
    [SerializeField] private float speed = 100.0f;

    //Walls
    private float verticalWall = 5f;
    private float horizontalWall = 7f;

    private Vector3 gravity = new Vector3(0f, -9.8f, 0f);

    //Radius
    private float radius = 0.5f;
    private float powerupRadius = 1f;

    private Vector3 velocity;

    //PowerUp
    public Transform powerUp;

    public static bool canResume;   

    [Header("Palettes")]
    [SerializeField] private FirstPaletteController palette1_Old = null;
    [SerializeField] private FirstPaletteController palette2_Old = null;

    [Header("Sound Settings")]
    [SerializeField] private AudioClip pongSound = null;
    [SerializeField] private AudioClip clapSound = null;

    private AudioSource audioSrc;

    private Animator animator;

    private float timeToScale = 5.0f;
    [SerializeField] private float currentTimeToScale = 0.0f;

    [SerializeField] private GameObject gameOverScreen = null;

    #endregion fields

    private void Awake()
    {
        audioSrc = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    public void Start()
    {
        velocity = new Vector3(speed, speed, 0); 
        canResume = true;
        powerUp.gameObject.SetActive(true);
    }

    public void GameOver()
    {
        transform.position = Vector3.zero;

        Time.timeScale = 0f;

        Points.rightPoints = 0;
        Points.leftPoints = 0;

        gameOverScreen.SetActive(true);
        powerUp.gameObject.SetActive(false);
    }

    void Update()
    {
        #region rebote con paredes y paletas

        if (canResume)
        {
            //====REBOTE CON PAREDES====//
            velocity += gravity * Time.deltaTime;
            transform.position 
                += velocity * Time.deltaTime 
                + 0.5f * gravity * Time.deltaTime * Time.deltaTime;

            if (transform.position.y >= verticalWall - radius) velocity.y = -Mathf.Abs(velocity.y);
            if (transform.position.y <= -verticalWall + radius) velocity.y = Mathf.Abs(velocity.y);

            //====REBOTE CON PALETA DERECHA====//
            if (transform.position.x >= horizontalWall)
            {
                float paletteY = palette1_Old.transform.position.y;
                if (transform.position.y <= paletteY + palette1_Old.high 
                    && transform.position.y >= paletteY - palette1_Old.high)
                {
                    velocity.x = -Mathf.Abs(velocity.x);

                    audioSrc.clip = pongSound;
                    audioSrc.Play();
                }

                else if (transform.position.x >= palette1_Old.transform.position.x)
                {
                    transform.position = Vector3.zero;
                    Points.rightPoints -= Points.rightPoints;

                    audioSrc.clip = clapSound;
                    audioSrc.Play();

                    Points.leftPoints += 1;
                }   
            }

            //====REBOTE CON PALETA IZQUIERDA====//
            if (transform.position.x <= -horizontalWall)
            {
                float paletteY2 = palette2_Old.transform.position.y;
                if (transform.position.y <= paletteY2 + palette2_Old.high 
                    && transform.position.y >= paletteY2 - palette2_Old.high)
                {
                    velocity.x = Mathf.Abs(velocity.x);

                    audioSrc.clip = pongSound;
                    audioSrc.Play();
                }

                else if (transform.position.x <= palette2_Old.transform.position.x)
                {
                    transform.position = Vector3.zero;
                    Points.leftPoints -= Points.leftPoints;

                    audioSrc.clip = clapSound;
                    audioSrc.Play();

                    Points.rightPoints += 1;
                }
            }

            #endregion rebote con paredes y paletas

            #region colisión con  powerUps

            //====COLISIÓN CON POWERUPS====//
            if (powerUp != null)
            { 
                float radiosPlus = radius + powerupRadius;

                if (Vector2.SqrMagnitude(transform.position - powerUp.position) < radiosPlus * radiosPlus)
                {
                    Destroy(powerUp.gameObject);
                    animator.SetBool("IsReady", true);
                }               
            } 

            else if (powerUp == null)
            {
                currentTimeToScale += Time.deltaTime;

                if (currentTimeToScale >= timeToScale)
                    animator.SetBool("IsReady", false);
            }

            if (Points.leftPoints == 3 || Points.rightPoints == 3)
            {
                GameOver();
            }

            #endregion colisión con powerUps
        }
    }
    /*
    void Count()
    {
        Debug.Log("entrando al void");

        canResume = false;

        Debug.Log("falseando");

        currentTimeToResume += timeToResume;

        if (currentTimeToResume >= timeToResume)
        {
            canResume = true;
        }
    }
    */
}

//Vector3 velocity = Vector3.zero;
/*
if (Input.GetKey(KeyCode.D))
{
    //x = x + vx * t
    velocity += Vector3.right * speed * Time.deltaTime;

    Debug.Log(transform.position);
}

if (Input.GetKey(KeyCode.A))
    velocity -= Vector3.right * speed * Time.deltaTime;

if (Input.GetKey(KeyCode.W))
    velocity += Vector3.up * speed * Time.deltaTime;

if (Input.GetKey(KeyCode.S))
    velocity += Vector3.down * speed * Time.deltaTime;
*/


