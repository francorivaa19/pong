﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private Button playButton = null;
    [SerializeField] private Button goBackButton = null;

    [SerializeField] private string sceneName = null;

    private AudioSource audioSrc = null;

    [SerializeField] private AudioClip buttonPressed = null;

    [SerializeField] private GameObject helpMenu = null;
    [SerializeField] private GameObject title = null;

    private void Awake()
    {
        audioSrc = GetComponent<AudioSource>();
        playButton.onClick.AddListener(OnPlayHandler);
        playButton.onClick.AddListener(OnHelpHandler);
    }

    private void Start()
    {
        helpMenu.gameObject.SetActive(false);
        title.gameObject.SetActive(true);
        goBackButton.gameObject.SetActive(false);
    }

    private void OnPlayHandler()
    {
        audioSrc.clip = buttonPressed;
        audioSrc.Play();

        SceneManager.LoadScene(sceneName);    
    }

    private void OnHelpHandler()
    {
        helpMenu.gameObject.SetActive(true);
        goBackButton.gameObject.SetActive(true);

        title.gameObject.SetActive(false);
        playButton.gameObject.SetActive(false);
    }
}
