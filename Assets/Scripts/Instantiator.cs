﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour
{
    [SerializeField] private float timeToInstantiate = 10.0f;
    [SerializeField] private float currentTimeToInstantiate = 0.0f;

    [SerializeField] private GameObject powerUp = null;

    private void Start()
    {
        currentTimeToInstantiate = timeToInstantiate;
    }

    void Update()
    {
        currentTimeToInstantiate += Time.deltaTime;

        if (currentTimeToInstantiate >= timeToInstantiate)
        {
            Instantiate(powerUp, transform.position, transform.rotation);
            currentTimeToInstantiate = 0.0f;
        }           
    }
}
