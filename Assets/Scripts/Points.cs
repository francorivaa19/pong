﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    public static int rightPoints = 0;
    public static int leftPoints = 0;

    [SerializeField] private Text rightPointsText = null;
    [SerializeField] private Text leftPointsText = null;

    void Update()
    {
        rightPointsText.text = rightPoints.ToString();
        leftPointsText.text = leftPoints.ToString();
    }
}
