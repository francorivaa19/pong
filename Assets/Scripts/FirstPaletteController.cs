﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPaletteController : MonoBehaviour
{
    [Header("Velocity & Acceleration Settings")]
    [SerializeField] private float desiredSpeed = 5.0f;
    //que tan rápido puedo ir
    public float maxVelocity = 0.0f;
    //que tan rápido cambia mi velocidad según las teclas
    public float inputAcceleration = 0.0f;   
    private float actualAcceleration;

    //cuanto acelera ahora mismo
    private Vector3 acceleration;
    private Vector3 velocity;

    private float horizontalWall = 5.0f;

    [Header("Palette Settings")]
    public float high = 3f;
    public float width = 1f;

    [Header("Buttons")]
    public KeyCode button;
    public KeyCode button2;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Points.leftPoints++;
            Debug.Log("im colisioning");
        }
    }

    void Start()
    {
        velocity = Vector3.zero;
    }

    void Update()
    {
        acceleration = Vector3.zero;
        desiredSpeed = 0.0f;

        if (Input.GetKey(button)) {
            desiredSpeed += maxVelocity;
        }

        if (Input.GetKey(button2)) {
            desiredSpeed -= maxVelocity;
        }

        if (velocity.y < desiredSpeed)
        {
            velocity.y += inputAcceleration * Time.deltaTime;

            if (velocity.y > desiredSpeed) 
                velocity.y = desiredSpeed;

            acceleration = Vector3.up * inputAcceleration;
            velocity += acceleration * Time.deltaTime;
        }

        if (velocity.y > desiredSpeed)
        {
            acceleration = Vector3.down * inputAcceleration;
            velocity.y -= inputAcceleration * Time.deltaTime;

            if (velocity.y < desiredSpeed) 
                velocity.y = desiredSpeed;
        }

        //FÓRMULA DE FÍSICA:
        //POSICIÓN(TIEMPO + 1) = POSICIÓN(TIEMPO) + VELOCIDAD * T + 1/2 * ACELERACIÓN * TIEMPO * TIEMPO
        //x(t+1) = x(t) + v * t + 1/2 * a * t * t;
        transform.position += velocity * Time.deltaTime + 0.5f * acceleration * Time.deltaTime * Time.deltaTime;

        if (transform.position.y >= horizontalWall - high)
        {
            transform.position = new Vector3(transform.position.x, horizontalWall - high);
            velocity.y = 0.0f;
        }

        if (transform.position.y <= -horizontalWall + high)
        {
            transform.position = new Vector3(transform.position.x, -horizontalWall + high);
            velocity.y = 0.0f;
        }    
    }


    
}
