﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ALT_SecondPalette : MonoBehaviour
{
    float speed = 8.0f;
    public float high = 1.5f;
    public float width = 1f;
    private float horizontalWall = 5.0f;

    private Vector3 velocity;

    void Start()
    {
        velocity = new Vector3(0, 0, 0);
    }

    void Update()
    {
        //MOVEMENT
        velocity = Vector3.zero;

        if (Input.GetKey(KeyCode.W)) transform.position += Vector3.up * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.S)) transform.position += Vector3.down * speed * Time.deltaTime;

        transform.position += velocity * Time.deltaTime;

        //LIMITS
        if (transform.position.y >= horizontalWall - high) transform.position = new Vector3(transform.position.x, horizontalWall - high);
        if (transform.position.y <= -horizontalWall + high) transform.position = new Vector3(transform.position.x, -horizontalWall + high);

    }
}
