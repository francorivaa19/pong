﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ALT_FirstPalette : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;

    private float horizontalWall = 5.0f;

    public float forceWithInput;
    public float mass = 0.0f;

    public float high = 1.5f;
    public float width = 1f;

    private Vector3 velocity;

    void Update()
    {
        //MOVEMENT
        velocity = Vector3.zero;
        Vector3 force = Vector3.zero;
        forceWithInput = 0.0f;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            force += Vector3.up * forceWithInput;
            transform.position += Vector3.up * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            force += Vector3.down * forceWithInput;
            transform.position += Vector3.down * speed * Time.deltaTime;
        }

        transform.position += velocity * Time.deltaTime;

        //LIMITS
        if (transform.position.y >= horizontalWall - high)
            transform.position = new Vector3(transform.position.x, horizontalWall - high);

        if (transform.position.y <= -horizontalWall + high) 
            transform.position = new Vector3(transform.position.x, -horizontalWall + high);
    }
}
