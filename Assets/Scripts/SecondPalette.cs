﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPalette : MonoBehaviour
{
    public float amplitude = 0.2f;
    
    public float high = 1.5f;
    public float width = 1f;

    //private Vector3 velocity;

    void Update()
    {
        transform.position += new Vector3(0, amplitude * Mathf.Sin(2f * Time.time), 0);
        /*
        //MOVEMENT
        velocity = Vector3.zero;

        if (Input.GetKey(KeyCode.W)) transform.position += Vector3.up * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.S)) transform.position += Vector3.down * speed * Time.deltaTime;

        transform.position += velocity * Time.deltaTime;

        //LIMITS
        if (transform.position.y >= horizontalWall - high) transform.position = new Vector3(transform.position.x, horizontalWall - high);
        if (transform.position.y <= -horizontalWall + high) transform.position = new Vector3(transform.position.x, -horizontalWall + high);
        */
    }
}
